require 'reg/reg.php';
	
class authTests extends PHPUnit_Framework_TestCase 
{		
	private $check;
 
	protected function setUp()
	{
		$this->check = new reg();
	}
 
	protected function tearDown()
	{
		$this->check = NULL;
	}
 
	public function testAdd()
	{
		$result = $this->check->check("artyom.myndrul@mail.ru","1234567");
		$this->assertEquals(2, $result);
	}
	
	public function testFailure()
	{	
		$result = $this->check->check("artyom.myndrul@mail.ru","1234567");
		$this->assertNotEmpty($result);
	}
}	
