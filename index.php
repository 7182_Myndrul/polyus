<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <title>Полюс возможностей</title>
    <link  rel="stylesheet" type="text/css" href= "style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet"> 
</head>	
    
<body>

    
    <header class="header">
        <div class="container">
            <div class="header_inner">
                <div class="header_logo" action="login/log.php">Полюс возможностей</div> 

				<?php if (($_COOKIE['admin'] == '') && ($_COOKIE['user'] == '')): ?>
                    <form class="nav" action="login/log.php">				
						<a id="window" class="nav_link" href="login/log.html">Вход</a> 
						<a id="window" class="nav_link" href="reg/registration.html">Регистрация</a>  
					</form>
                <?php endif;?>
                
                <?php if($_COOKIE['admin'] != ''): ?>
                    <form class="nav" action="login/log.php">		
                        <a id="window" class="nav_link" href="statuser.php">Статистика</a>
                        <a id="window" class="nav_link" href="login/exit.php">Выход</a>
                    </form>
                <?php endif;?>
                
                <?php if(($_COOKIE['admin'] == '') && ($_COOKIE['user'] != '')): ?>
                    <form class="nav" action="login/log.php">	
                        <a id="window" class="nav_link" href="status.php">Статус</a>
                        <a id="window" class="nav_link" href="login/exit.php">Выход</a>
                    </form>
				<?php endif;?>
				
            </div>          
        </div>  
    </header>
    
    
    <footer class="footer">
        <div class="container">
             <div class="contact_logo">Контакты</div>
            <div class="footer_inner">
               
                <div class="contact" >Почта:</div> 
                <a class="contact_pochta" href="https://mail.google.com/mail/u/0/#inbox">notaxelin@gmail.com</a>
            </div>
        </div>
    </footer>
   
    
    <div class="intro">
        <div class='container'>
        </div>
    </div>
  
</body>	
</html>
