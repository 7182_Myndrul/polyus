<?php
	$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_STRING);
    $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_STRING);
	$password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);
	$name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
	$surname = filter_var(trim($_POST['surname']), FILTER_SANITIZE_STRING);
	$vacancy = filter_var(trim($_POST['vacancy']), FILTER_SANITIZE_STRING);
	$phone = filter_var(trim($_POST['phone']), FILTER_SANITIZE_STRING);
	$stazh = filter_var(trim($_POST['stazh']), FILTER_SANITIZE_STRING);	
	$status = filter_var(trim($_POST['status']), FILTER_SANITIZE_STRING);	

	    // Параметры для подключения
	
	try {
		$pdo = new PDO('mysql:dbname=polyus; host=polyus', 'root', 'root');
	} catch (PDOException $e) {
		die($e->getMessage());
	}
	
	if (isset($_POST['status'])) {
		$sql4 = $pdo->query("INSERT INTO `reg_humans` status VALUES '$status'");
	}
	// Create

	if (isset($_POST['submit'])) {
		$sql = ("INSERT INTO `reg_humans` (email, name, surname, vacancy, phone, stazh) VALUES ('$email', '$name', '$surname', '$vacancy', '$phone', '$stazh')");
		$query = $pdo->prepare($sql);
		$query->execute([$email, $name, $surname, $vacancy, $phone, $stazh]);
		$success = '<div class="alert alert-success alert-dismissible fade show" role="alert">
	  <strong>Данные успешно отправлены!</strong> Вы можете закрыть это сообщение.
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	  </button>
	</div>';
		
	}

	// Read

	$sql = $pdo->prepare("SELECT * FROM reg_humans");
	$sql->execute();
	$result = $sql->fetchAll();

	// Update
	$edit_email = $_POST['edit_email'];
	$edit_name = $_POST['edit_name'];
	$edit_surname = $_POST['edit_surname'];
	$edit_vacancy = $_POST['edit_vacancy'];
	$edit_stazh = $_POST['edit_stazh'];
	$edit_phone = $_POST['edit_phone'];
	$edit_status = $_POST['edit_status'];
	$get_id = $_GET['id'];
	
	if (isset($_POST['edit-submit'])) {
		$sqll = "UPDATE reg_humans SET email=?, name=?, surname=?, vacancy=?, stazh=?, phone=?, status=? WHERE id=?";
		$querys = $pdo->prepare($sqll);
		$querys->execute([$edit_email, $edit_name, $edit_surname, $edit_vacancy, $edit_stazh, $edit_phone, $edit_status, $get_id]);
		header('Location: '. $_SERVER['HTTP_REFERER']);
	}

	// DELETE
	if (isset($_POST['delete_submit'])) {
		$sql = "DELETE FROM reg_humans WHERE id = ?";
		$query = $pdo->prepare($sql);
		$query->execute([$get_id]);
	}
?>